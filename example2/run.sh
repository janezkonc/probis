#!/bin/bash

probisexe=/v/apps/probis/gsl2-ompi184/probis

$probisexe -compare -super -dist 3.0 -f1 1phr.pdb -c1 A -bsite1 SO4.158.A -f2 3jvi.pdb -bsite2 SO4.201.A -c2 A

##### ALTERNATIVE
#
#$probisexe -compare -super -motif1 "[:A and (12-19,129-131)]" -motif2 "[:A and (7-15)]" -f1 1phr.pdb -c1 A -f2 3jvi.pdb -c2 A
#
#