########################
#        EXAMPLE 2     #
########################

Want to align two binding sites only?
There are two options: -bsite or -motif

The superimposed protein will be in rota.pdb file. 
To see the superimposition, use e.g.:

pymol 1phrA_3jviA.0.rota.pdb 1phr.pdb

To start this example: ./run.sh
