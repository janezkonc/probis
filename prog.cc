#ifdef MPI_ENABLED
#include "ParallelFramework.h"
#endif // MPI_ENABLED

#include "args.h"
#include "const.h"
#include "states.h"

int main(int argc, char *argv[]) {
#ifdef MPI_ENABLED
  Mpi::Environment env(argc, argv);
  Mpi::Buffer mpiBuf(100 * 1000 * 1000); // *1000 is a hack to avoid errors
#endif                                   // MPI_ENABLED

  try {
    Args args;
    args.fill_args(argc, argv);
    args.print_state();
    args.print_modifiers();
    args.print_constants();
    switch (_state) {
    case TOTAL:
      state0(PROTEIN2, CHAIN2);
      break;
      //    case PPI    : state1(args);       break;
      //    case BIND   : state2(args);       break;
    case WRITE:
      state3(&args);
      break;
    case SURFDB:
      state4(&args);
      break;
    case MARK:
      state5(&args);
      break;
    case LIGAND:
      state6(&args);
      break;
    case RESULTS:
      state7(&args);
      break;
      //  case SEQ    : state8(args);       break;
    case ALIGN:
      state9(&args);
      break;
      //  case BIOU   : state10(args);       break;
    case HEAD:
      state11(&args);
      break;
    case ALLBIO:
      state12(&args);
      break;
#ifdef CILE
    case PATCH:
      stateX(&args);
      break;
#endif
    }
  } // .. konec try bloka
  catch (Err e) {
    cout << e.what() << endl;
  }

  return 0;
}
