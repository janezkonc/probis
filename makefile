#
#       Makefile to compile ProBiS
#
#       Usage: 
#              To compile the program:           make probis
#              To compile the library:           make library
#              To clean object files:            make clean
#
#

# Set compiler CC: uncomment depending on system Linux or Mac
# for LINUX
#CC = g++
# for MAC
CC = clang++

RM	= rm -f
ECHO	= echo

# UNCOMMENT BELOW FOR THREADS
MPICC = $(CC)

# Set CFLAGS: uncomment depending on system Linux or Mac
# for LINUX
#CFLAGS = -std=c++0x -O3 -static -Wl,--whole-archive -lpthread -Wl,--no-whole-archive -Wall -Wno-char-subscripts -DNDEBUG
# for MAC
# here assuming that homegrew is used to install th gsl packages
# location of include/ and lib/ library might be different on your Mac
CFLAGS = -std=c++11 -O3 -Wall -Wno-unused-function -DNDEBUG -I/opt/homebrew/Cellar/gsl/2.7.1/include -L/opt/homebrew/Cellar/gsl/2.7.1/lib

########################

## UNCOMMENT BELOW FOR MPI
#MPICC = mpic++
#CFLAGS = -std=c++0x -O3 -Wall -Wno-char-subscripts -DMPI_ENABLED -DNDEBUG
#########################

# DEFINITIONS FOR STATIC LIBRARY (ONLY COMPATIBLE WITH THREADS)
AR	= ar
ARFLAGS	= 
RANLIB	= ranlib
############################


OBJLIBS	= libProBiS_static.a


GSL_FLAGS = -lgsl -lgslcblas -lm


OBJS = ligands.o bsite.o eelement.o probe.o \
        colors.o sphere.o atom.o geo.o grid.o \
        residue.o ligand.o molecule.o desc.o \
        schmitt.o item.o output.o kabsch.o score.o bit.o \
        clique.o subgraph.o product.o cluster.o \
        clusterlib.o args.o const.o states.o prog.o \
        motif.o denorm.o nosql.o parallel.o ProBiS.o

# first define target file : student 
#              dependencies are the object files that build the program
library : $(OBJLIBS)

probis: $(OBJS)
	$(MPICC) $(CFLAGS) \
        -o probis $(OBJS) $(GSL_FLAGS)

# now define how each object file is a target and list dependencies and how
#  to build that object file if any dependencies change

ProBiS.o: ProBiS.cpp ProBiS.hpp states.h args.h \
        const.h error.hpp debug.hpp
	$(CC) $(CFLAGS) -c ProBiS.cpp

ligands.o: ligands.cc ligands.h molecule.h \
	   ligand.h residue.h clusterlib.h const.h \
           geo.h bsite.h grid.h clusterdata.h \
           kabsch.h
	$(CC) $(CFLAGS)  -c ligands.cc

bsite.o: bsite.cc bsite.h const.h geo.h ligand.h
	$(CC) $(CFLAGS)  -c bsite.cc

motif.o: motif.cc motif.h const.h
	$(CC) $(CFLAGS)  -c motif.cc

denorm.o: denorm.cc denorm.h geo.h kabsch.h clusterdata.h \
	  residue.h const.h
	$(CC) $(CFLAGS)  -c denorm.cc

eelement.o: eelement.cc eelement.h const.h
	$(CC) $(CFLAGS)  -c eelement.cc

probe.o: probe.cc probe.h const.h sphere.h
	$(CC) $(CFLAGS)  -c probe.cc

colors.o: colors.cc colors.h
	$(CC) $(CFLAGS)  -c colors.cc

sphere.o: sphere.cc sphere.h
	$(CC) $(CFLAGS)  -c sphere.cc

atom.o: atom.cc atom.h const.h eelement.h colors.h sphere.h \
	geo.h
	$(CC) $(CFLAGS)  -c atom.cc

geo.o: geo.cc geo.h const.h
	$(CC) $(CFLAGS)  -c geo.cc

grid.o: grid.cc grid.h desc.h const.h geo.h atom.h probe.h \
	eelement.h ligand.h
	$(CC) $(CFLAGS)  -c grid.cc

residue.o: residue.cc residue.h const.h
	$(CC) $(CFLAGS)  -c residue.cc

ligand.o: ligand.cc ligand.h const.h molecule.h eelement.h \
          sphere.h clusterdata.h
	$(CC) $(CFLAGS)  -c ligand.cc

molecule.o: molecule.cc molecule.h desc.h const.h atom.h \
	kabsch.h eelement.h probe.h \
        biounit.h ligands.h ligand.h residue.h geo.h grid.h \
        item.h output.h clusterdata.h motif.h
	$(CC) $(CFLAGS)  -c molecule.cc

desc.o: desc.cc desc.h sphere.h product.h const.h \
        schmitt.h atom.h eelement.h grid.h probe.h
	$(CC) $(CFLAGS)  -c desc.cc

schmitt.o: schmitt.cc schmitt.h
	$(CC) $(CFLAGS)  -c schmitt.cc

item.o: item.cc item.h vert.h product.h \
        const.h
	$(CC) $(CFLAGS)  -c item.cc

output.o: output.cc output.h atom.h item.h molecule.h \
          product.h desc.h score.h const.h \
          eelement.h probe.h
	$(CC) $(CFLAGS)  -c output.cc

kabsch.o: kabsch.cc kabsch.h geo.h item.h desc.h product.h \
          const.h atom.h
	$(CC) $(CFLAGS)  -c kabsch.cc

score.o: score.cc score.h kabsch.h item.h molecule.h subgraph.h \
         desc.h probe.h product.h eelement.h atom.h const.h
	$(CC) $(CFLAGS)  -c score.cc

bit.o: bit.cc bit.h const.h
	$(CC) $(CFLAGS)  -c bit.cc

clique.o: clique.cc clique.h kabsch.h const.h item.h subgraph.h \
          bit.h
	$(CC) $(CFLAGS)  -c clique.cc

subgraph.o: subgraph.cc subgraph.h item.h vert.h molecule.h \
            const.h atom.h product.h kabsch.h output.h \
            desc.h score.h
	$(CC) $(CFLAGS)  -c subgraph.cc

product.o: product.cc product.h desc.h item.h atom.h eelement.h \
           score.h const.h
	$(CC) $(CFLAGS)  -c product.cc

cluster.o: cluster.cc cluster.h product.h subgraph.h item.h \
           const.h
	$(CC) $(CFLAGS)  -c cluster.cc

nosql.o: nosql.cc nosql.h molecule.h residue.h clusterdata.h \
         denorm.h item.h subgraph.h product.h \
         desc.h atom.h const.h
	$(CC) $(CFLAGS)  -c nosql.cc

clusterlib.o: clusterlib.cc clusterlib.h
	$(CC) $(CFLAGS)  -c clusterlib.cc

args.o: args.cc args.h const.h geo.h motif.h
	$(CC) $(CFLAGS)  -c args.cc

const.o: const.cc const.h args.h geo.h ligands.h product.h \
         grid.h probe.h desc.h clique.h subgraph.h cluster.h \
         motif.h molecule.h output.h score.h nosql.h
	$(CC) $(CFLAGS)  -c const.cc

parallel.o: ParallelFramework.h MpiWrapper.h Timer.h parallel.h \
         parallel.cc geo.h
	$(MPICC) $(CFLAGS)  -c parallel.cc

states.o: states.cc states.h molecule.h grid.h desc.h \
          score.h probe.h clique.h product.h args.h const.h \
          subgraph.h cluster.h output.h ligands.h item.h \
          motif.h nosql.h parallel.h
	$(MPICC) $(CFLAGS)  -c states.cc

prog.o: prog.cc states.h args.h \
        const.h 
	$(MPICC) $(CFLAGS)  -c prog.cc

#    
#    
#    

libProBiS_static.a : $(OBJS)
	$(ECHO) $(AR) $(ARFLAGS) rv libProBiS_static.a $?
	$(AR) $(ARFLAGS) rv libProBiS_static.a $?
	$(ECHO) $(RANLIB) libProBiS_static.a
	$(RANLIB) libProBiS_static.a



clean:
	$(RM) probis *.o *~ *.a


# that's all
