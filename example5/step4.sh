#!/bin/bash

# Step 4: Get superimposed proteins as .pdb files. 
#
# We get only the best (first) out of the alternative 
# alignments. The output .rota.pdb files contain both proteins
# superimposed - each one in its MODEL/ENDMDL section.. 
#
# Important: residues that were superimposed are marked with 1.00 beta factor
# For example:
#
# ATOM     98  N   SER A  14       6.596  31.642  25.895  1.00  1.00           N  
#

probisexe=/v/apps/probis/gsl2-ompi184/probis

for i in $(cat proteins.txt); 
do
    $probisexe -align -alno 0 -f1 1phr.pdb -c1 A -f2 ${i:0:4}.pdb -c2 ${i:4:1} -nosql example.nosql
done