########################
#        EXAMPLE 5     #
########################

Compare a query binding site against a database of template binding sites.

To start this example, run the scripts step1.sh - step4.sh

Step 1: Extract the surface of the SO4 binding site on 1phrA
(residues within 3.0 Angstrom of SO4 are labeled as binding site)

Alternatively, binding site can be defined using residue ids and chain id.

Extract binding sites for other proteins as well - see proteins.txt file
where the codes for ligands are defined!!

Step 2: Compare 1phr's SO4 binding site against other
protein structures (surfaces).

Step 3: Postprocessing : convert alignments in nosql to json format

Step 4: Get superimposed proteins as .pdb files. 

We get only the best (first) out of the alternative 
alignments. The output .rota.pdb files contain both proteins
superimposed - each one in its MODEL/ENDMDL section.. 

Important: residues that were superimposed are marked with 1.00 beta factor
For example:

ATOM     98  N   SER A  14       6.596  31.642  25.895  1.00  1.00           N  

