#!/bin/bash

# Step 1: Extract the surface of the SO4 binding site on 1phrA
# (residues within 3.0 Angstrom of SO4 are labeled as binding site)
#
# Alternatively, binding site can be defined using residue ids and chain id.
#
# Other protein surfaces are extracted whole (as in the previous example)
# from .pdb proteins to .srf files.

probisexe=/v/apps/probis/gsl2-ompi184/probis

$probisexe -extract -bsite SO4.158.A -dist 3.0 -f1 1phr.pdb -c1 A -srffile 1phrA.srf 

###### ALTERNATIVE:
#$probisexe -extract -motif "[:A and (12-19,129-131)]" -f1 1phr.pdb -c1 A -srffile 1phrA.srf 
#################

for i in $(cat proteins.txt); 
do
    $probisexe -extract -f1 ${i:0:4}.pdb -c1 ${i:4:1} -srffile $i.srf; 
done
