#!/bin/bash

# Step 3: Postprocessing : convert alignments in nosql to json format

probisexe=/v/apps/probis/gsl2-ompi184/probis

$probisexe -results -f1 1phr.pdb -c1 A -nosql example.nosql -json example.json
