########################
#        EXAMPLE 1     #
########################

This example shows how to superimpose two protein structures 
(i.e., their entire surfaces). The algorithm extracts their surfaces and generates
multiple superimpositions of the two compared proteins.
The output is written to #.rota.pdb files, where number # is the rank
of the superimposition. Superimposition 0 is always the best one
others are alternatives... 

To start this example: ./run.sh
