#!/bin/bash

# Step 3: Postprocessing : convert alignments in nosql to json format
# This step is NOT necessary for this example, however, json format
# is somewhat easier to read by humans.

probisexe=/v/apps/probis/gsl2-ompi184/probis

$probisexe -results -f1 1phr.pdb -c1 A -nosql example.nosql -json example.json
