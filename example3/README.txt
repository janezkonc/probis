########################
#        EXAMPLE 3     #
########################

Compare a query protein against a database of template proteins.

To start this example, run the scripts step1.sh - step4.sh

Step 1: extract the surfaces of .pdb proteins to 
.srf files

Step 2: Compare 1phr (whole protein surface) against other
protein structures (surfaces).

Step 3: Postprocessing : convert alignments in nosql to json format
This step is NOT necessary for this example, however, json format
is somewhat easier to read by humans.

Step 4: Get superimposed proteins as .pdb files. 
We get only the best (first) out of the alternative 
alignments. The output .rota.pdb files contain both proteins
superimposed - each one in its MODEL/ENDMDL section.. 
