#!/bin/bash

# Step 1: extract the surfaces of .pdb proteins to 
# .srf files

probisexe=/v/apps/probis/gsl2-ompi184/probis

$probisexe -extract -f1 1phr.pdb -c1 A -srffile 1phrA.srf 

for i in $(cat proteins.txt); 
do
    $probisexe -extract -f1 ${i:0:4}.pdb -c1 ${i:4:1} -srffile $i.srf; 
done