#include "ProBiS.hpp"
#include "args.h"
#include "const.h"
#include "debug.hpp"
#include "error.hpp"
#include "states.h"

using namespace std;

namespace ProBiS {
void compare_against_bslib(int argc, char *argv[], const string &receptor_file,
                           const string &receptor_chain_id,
                           const string &bslib_file, const int ncpu,
                           const string &nosql_file, const string &json_file) {
#ifndef MPI_ENABLED

  try {
    cout << "Starting ProBiS for binding sites prediction" << endl;
    Args args;
    _longnames = true;
    _local = true;
    _just_align = true; // when library call, just output JSON alignments file

    NCPU = ncpu;
    size_t found = receptor_file.find_last_of(".");

    SRF_FILE = receptor_file.substr(0, found) + ".srf";
    PROTEIN1 = receptor_file;
    CHAIN1 = receptor_chain_id;
    SURF_FILE = bslib_file.c_str();
    NOSQL_FILE = nosql_file;
    JSON_FILE = json_file;

    args.print_state();
    args.print_modifiers();
    args.print_constants();

    state3(&args);

    _srf = true;
    PROTEIN1 = SRF_FILE;

    state4(&args);
    PROTEIN1 = receptor_file;

    state7(&args);

  } // .. konec try bloka
  catch (Err e) {
    cout << e.what() << endl;
    throw Error("die : something went wrong in probis ...");
  }

#endif
}

}; // namespace ProBiS
